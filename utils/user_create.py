import os
import csv
import crypt

test = True

with open('users.csv', 'r', newline='') as csvfile:
  spamreader = csv.reader(csvfile, delimiter=';', quotechar='|')
  for row in spamreader:
    user_name = row[0]
    password = row[1]
    encrypted_password = crypt.crypt(password, salt=user_name)
    addUser = "sudo useradd -m  " + user_name + " -p " + encrypted_password
    addToDockerGroup = "sudo usermod -aG docker " + user_name
    if not test:
      # Create user
      os.system(addUser)
      # Add user to docker group
      os.system(addToDockerGroup)
    else:
      print(addUser)
      print(addToDockerGroup)