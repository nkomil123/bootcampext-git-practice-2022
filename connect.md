Адрес стенда - 51.250.27.32
Программа для доступа по ssh из windows - [putty](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html)
Подключаемся под своим логином/паролем

Нужно подключиться и проверить, что `git` и `docker` работает.

```bash
# Смотрим верcию git
$ git --version
# Запускаем контейнер hello-world
$ docker run hello-world
```

Вывод должен быть примерно такой:

```bash
login as: red00
red00@51.250.27.32's password:
Last login: Mon Jul 18 04:27:51 2022 from 1.1.1.1
[red00@srv-jenkins-agent ~]$ git --version
git version 1.8.3.1
[red00@srv-jenkins-agent ~]$ docker run hello-world

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/

[red00@srv-jenkins-agent ~]$

```