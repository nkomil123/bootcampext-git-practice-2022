alembic==1.0.0
click==6.7
Flask==1.0.2
Flask-Migrate==2.2.1
Flask-SQLAlchemy==2.3.2
itsdangerous==0.24
Jinja2==2.10
Mako==1.0.7
MarkupSafe==1.1.1
python-dateutil==2.7.3
python-dotenv==0.8.2
python-editor==1.0.3
six==1.11.0
SQLAlchemy==1.2.10
Werkzeug==0.14.1
gunicorn==20.0.4
# Test Env
flake8==3.7.9
yapf # we want recieve new versions
autopep8==1.4.4
radon==4.0.0
bandit==1.7.2
coverage==4.5.4
